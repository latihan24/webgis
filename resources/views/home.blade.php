<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
        integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
        integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
    
    <script src="https://unpkg.com/esri-leaflet@^3.0.9/dist/esri-leaflet.js"></script>
    <script src="https://unpkg.com/esri-leaflet-vector@4.0.0/dist/esri-leaflet-vector.js"></script>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="{{ asset('leaflet/leaflet.textpath.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('leaflet/markerCluster.css') }}"/>
    <link rel="stylesheet" href="{{ asset('leaflet/markerCluster.Default.css') }}"/>
    <script src="{{ asset('leaflet/leaflet.markercluster.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('leaflet/Control.MiniMap.min.css') }}"/>
    <script src="{{ asset('leaflet/Control.MiniMap.min.js') }}"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/gokertanrisever/leaflet-ruler@master/src/leaflet-ruler.css" integrity="sha384-P9DABSdtEY/XDbEInD3q+PlL+BjqPCXGcF8EkhtKSfSTr/dS5PBKa9+/PMkW2xsY" crossorigin="anonymous">  
    <script src="https://cdn.jsdelivr.net/gh/gokertanrisever/leaflet-ruler@master/src/leaflet-ruler.js" integrity="sha384-N2S8y7hRzXUPiepaSiUvBH1ZZ7Tc/ZfchhbPdvOE5v3aBBCIepq9l+dBJPFdo1ZJ" crossorigin="anonymous"></script>

    <script src="{{ asset('leaflet/leaflet-hash.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('leaflet/L.Control.MousePosition.css') }}"/>
    <script src="{{ asset('leaflet/L.Control.MousePosition.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('leaflet/leaflet-routing-machine.css') }}"/>
    <script src="{{ asset('leaflet/leaflet-routing-machine.min.js') }}"></script>
    <script src="{{ asset('leaflet/Control.Geocoder.js') }}"></script>

    <script src="{{ asset('leaflet/config.js') }}"></script>

    <style>
        #map {
            height: 500px;
        }
        .Label-bidang{
            font-size:10pt;
            color:#04e2d4;
            text-align: center;
        }
        .Legend{
            background: white;
            padding:10px;
        }
    </style>
</head>

<body>
    <div>
        <p>cari lokasi</p>
        <select onchange="cari(this.value)">
            @foreach ($lokasi as $d)
            <option value="{{ $d->id }}">{{ $d->nama }}</option>
            @endforeach
            
        </select>
    </div>
    <br>
    <div>
        <input onclick="pilihsungai(this)" type="checkbox">sungai
        <input onclick="pilihbidang(this)" type="checkbox"  >bidang
    </div>
    <br>
    <div>
        <input id="titik_a" type="text">
        <input id="titik_b" type="text">
        <input id="jarak" type="number">
        <input id="jalan" type="text">
    </div>
    <br>
    <div id="map"></div>
</body>
<script>
    var map = L.map('map').setView([-6.9772027, 107.6597198], 18);

//L.esri.basemapLayer('Oceans').addTo(map);
//Hybrid: s,h;
//Satellite: s;
//Streets: m;
//Terrain: p;
//cari disini https://stackoverflow.com/questions/33343881/leaflet-in-google-maps
//L.tileLayer('http://{s}.google.com/vt?lyrs=s&x={x}&y={y}&z={z}',{
  //  maxZoom: 20,
  //  subdomains:['mt0','mt1','mt2','mt3']
//}).addTo(map);
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{
    maxZoom: 20,
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>contributors'
}).addTo(map);



var hotelIcon = L.icon({
    iconUrl: '{{asset('image/motel.png')}}',

    iconSize:     [25, 28], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

//untuk informasi tempat
//var marker = L.marker([-6.9772027, 107.6597198], { icon: hotelIcon}).addTo(map).on('click',function(e){

    //alert(e.latlng);
//});
//var marker = L.marker([-6.9772027, 107.6597198], { icon: hotelIcon}).addTo(map);

var latlngs = [
            [
                -6.96951544360121,
                107.66542093136422
          ],
          [
              -6.972780929526181,
            107.66311806704732
          ],
          [
              -6.97471052414717,
            107.65839270910021
          ]
];

var polyline = L.polyline(latlngs, {color: 'red'}).addTo(map);

polyline.setStyle(
    {
        weight:25,
        lineCap:'round' //butt/round/square
    }
);

polyline.on('click',(e)=>{
//alert(e.latlng);
polyline.setStyle({
    color: 'yellow'
});

})

//POLYGON
var latlngs = [
    [
    -6.969548980522362,
            107.66490574420857
          ],
          [
              -6.970904097948349,
            107.66200467890644
          ],
          [
              -6.973021461082595,
            107.66456444240828
          ],
          [
              -6.9695226263216625,
            107.66489750190743
          ]];

var polygon = L.polygon(latlngs, {color: 'red'}).addTo(map);
polygon.setStyle(
    {
        color: 'yellow',
        fillColor:'blue',
        weight:10,
        lineCap:'round' //butt/round/square
    }
);

// zoom the map to the polyline
map.fitBounds(polyline.getBounds());

polygon.on('click',(e)=>{
alert("ini polygon");


})
$( document ).ready(function() {
    $.getJSON('titik/json',function(data){
        $.each(data, function (index) {
            
            var hotelIcon = L.icon({
    iconUrl: '{{asset('image/motel.png')}}',

    iconSize:     [25, 28], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});
            L.marker([data[index].longitude,data[index].latitude],{icon:hotelIcon}).addTo(map);
        });
    });
});

var geoLayer;
var lyr_bidang = L.markerClusterGroup();
$.getJSON('{{asset('image/geojson/map.geojson')}}',function(json){
        geoLayer = L.geoJson(json,{
            style: function(feature){
                return{
                    fillOpacity: 0,
                    weight: 5,
                    opacity: 1,
                    color: "#008cff",
                    dashArray: "30 10",
                    lineCap: 'square'
                };
            },
            onEachFeature: function(feature, layer){
                //alert(feature.properties.nama)
                var iconLabel = L.divIcon({
                    className: 'label-bidang',
                    html: '<b>'+feature.properties.nama+'</b>',
                    iconSize: [100.20]

                });
                var marker = L.marker(layer.getBounds().getCenter(),{icon:iconLabel});//.addTo(map);
                layer.on('click',(e)=>{
                    $.getJSON('titik/lokasi/'+feature.properties.id,function(detail){
                        $.each(detail, function (index) {
                            //alert(detail[index].alamat);
                            var html='<h5> Nama Lokasi :'+detail[index].nama+'<h5>';
                                html+='<h6>Alamat:'+detail[index].alamat+'</h6>';
                                L.popup()
                                .setLatLng(layer.getBounds().getCenter())
                                .setContent(html)
                                .openOn(map);
                        });
                    });



                    })
                    lyr_bidang.addLayer(marker);
                    lyr_bidang.addLayer(layer);
                //layer.addTo(map);
            }
        });
    });


 var lyr_sungai = L.markerClusterGroup();
    $.getJSON('{{asset('image/geojson/sungai.geojson')}}',function(json){
        geoLayer = L.geoJson(json,{
            style: function(feature){
                return{
                    weight: 5,
                    opacity: 1,
                    color: "yellow",
                    dashArray: "30 10",
                    lineCap: 'square'
                };
            },
            onEachFeature: function(feature, layer){
                //alert(feature.properties.nama)
                layer.setText(feature.properties.nama ,{repeat:false, offset: -5, oriesntation:"angle", attributes:{style:"font-size:50pt",fill:"yellow"}});
                layer.on('click',(e)=>{
                    layer.setStyle(
                        {
                             color: 'red',
                             fillColor:'blue',
                             weight:10,
                             lineCap:'round' //butt/round/square
                        }
                    );


                    })
                //layer.addTo(map);
                lyr_sungai.addLayer(layer);
            }
        });
    });

    function cari(id){
        geoLayer.eachLayer(function(layer){
            if(layer.feature.properties.id==id){
                map.flyTo(layer.getBounds().getCenter(),19);
            }
        });
    }

    var legend =L.control({position:'bottomright'});
    legend.onAdd = function (map) {
        var div = L.DomUtil.create('div','Legend');
        labels = ['<strong>Keterangan:</strong>'],
        categories =['Rumah Sakit','Gedung Sekolah','Gedung Pemerintah'];
        for (var i =0; i< categories.length; i++){
            if (i==0){
                div.innerHTML +=
                labels.push(
                    '<img width="20"height="23" src="{{asset('image/motel.png')}}"><i class="circle" style="backgroun:#000000"></i>'+
                    (categories[i]?categories[i]: '+'));
            }else if (i==1){
                div.innerHTML +=
                labels.push(
                    '<img width="20"height="23" src="{{asset('image/motel.png')}}"><i class="circle" style="backgroun:#000000"></i>'+
                    (categories[i]?categories[i]: '+'));
            }else{
                div.innerHTML +=
                labels.push(
                    '<img width="20"height="23" src="{{asset('image/motel.png')}}"><i class="circle" style="backgroun:#000000"></i>'+
                    (categories[i]?categories[i]: '+'));
            }
        }
        div.innerHTML = labels.join('<br>');
        return div;
    };
    legend.addTo(map);


    //layer
    function pilihsungai (v){
        if(v.checked){
            map.addLayer(lyr_sungai);
        }else {
            map.removeLayer(lyr_sungai);
        }
    }
    function pilihbidang (v){
        if(v.checked){
            map.addLayer(lyr_bidang);
        }else {
            map.removeLayer(lyr_bidang);
        }
    }

    //MiniMap
    var oSatelite = L.tileLayer('http://{s}.google.com/vt?lyrs=s&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
});
var miniMap = new L.Control.MiniMap(oSatelite,{
    toggleDisplay: true,
    minimized: true
}).addTo(map);

//distanceMeansurement
L.control.ruler({
    position: 'topleft', 
}).addTo(map);


//HASH
var hash = new L.Hash(map);

//cursor
L.control.mousePosition().addTo(map);

//ROuteMechine
var waypoints =[
    L.latLng(-6.152702328013234, 106.56780450959889),
    L.latLng(-6.152692239244786, 106.56926268579457)
];
var geocoder= L.Control.Geocoder.nominatim()
	
var routeControl = L.Routing.control({
    geocoder: geocoder,
    waypoints: waypoints,
    routewhileDragging: true,
    lineOptions: {
        styles: [{ color:'green', opacity: 1, weight:5}]
    },
}).addTo(map);

routeControl.on('routesfound', function (e)  {
    console.log(e.routes[0]);
    
    var distance = e.routes[0].summary.totalDistance;
    var time = e.routes[0].summary.totalTime;

    document.getElementById("titik_a").value =e.routes[0].waypoints[0].latLng;
    document.getElementById("titik_b").value =e.routes[0].waypoints[1].latLng;
    document.getElementById("jalan").value =e.routes[0].name;
    document.getElementById("jarak").value =e.routes[0].summary.totalDistance;

});

</script>

</html>