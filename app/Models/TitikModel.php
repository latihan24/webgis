<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TitikModel extends Model
{
    public function allData(){
        $result = DB::table('tbl_titik')
        ->select ('nama', 'latitude', 'longitude')
        ->get();
        return $result;
    }
    public function getLokasi($id=''){
        $result = DB::table('tbl_lokasi')
        ->select ('nama', 'alamat', 'gambar')
        ->where('id',$id)
        ->get();
        return $result;
    }
    public function allLokasi(){
        $result = DB::table('tbl_lokasi')
        ->select ('id','nama')
        ->get();
        return $result;
    }
}
