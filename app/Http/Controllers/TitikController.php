<?php

namespace App\Http\Controllers;

use App\Models\TitikModel;
use Illuminate\Http\Request;

class TitikController extends Controller
{
    public function __construct(){
        $this->TitikModel=new TitikModel();
    }
    public function index(){
        $result=$this->TitikModel->allLokasi();
        return view('home',['lokasi'=>$result]);
    }
    public function titik(){
        $result=$this->TitikModel->allData();
        return json_encode($result);
    }
    public function lokasi($id){
        $result=$this->TitikModel->getLokasi($id);
        return json_encode($result);
    }

}
